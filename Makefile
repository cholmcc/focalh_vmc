#
#
#
srcdir		:=
ROOT		:= root
ROOT_FLAGS	:= -l -b
include	Files.mk
OTHER		:= Straws.ipynb			\
		   straws.py			\
		   README.md			\
		   Makefile

%.pdf:%.ipynb
	jupyter nbconvert --to pdf $<

help:; @$(info $(helpmsg)) :

build:		$(SIM_SOURCES)
	$(ROOT) $(ROOT_FLAGS) -q $<\(0,0,0\)

clean:
	rm -f *~ *.log
	rm -f geometry.root
	rm -f *_C*.*
	rm -f *.dat
	rm -rf __pycache__ .ipynb_checkpoints
	rm -rf focal focal.tar.gz

realclean:clean
	rm -f *.root
	rm -f *.pdf
	rm -rf focalh_vmc
	rm -f  focalh_vmc.tar.gz

dist:
	mkdir -p focalh_vmc/data
	cp $(SIM_SOURCES) $(ANA_SOURCES) $(SCRIPTS) $(OTHER) focalh_vmc
	cp data/.rootrc data/Makefile focalh_vmc/data/
	tar -czvf focalh_vmc.tar.gz focalh_vmc
	rm -rf focal

define helpmsg = 
- HOW TO USE MAKE WITH THIS PROJECT

    make TARGET [VAR=VALUE]*

"generates" TARGET, which must be a valid target. If not TARGET is
given, it defaults to "help".  A variable VAR can be set to a VALUE
directly on the command line.

Valid targets for this project are

- help - show this help

- build - builds the code but does nothing else

- Straws.pdf - generate PDF of the Jupter Notebook Straws.ipynb

- clean - clean all built files, such as shared libraris, back-up
  files, logs, etc.  This _does not_ delete any generate ROOT files

- realclean - same as clean, but also deletes ROOT files

- dist - Creates a tar-ball distribution of the project

More targets for data generation are available in the "data" sub-directory.
endef




.PHONY:	help





