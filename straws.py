from matplotlib import rcParams 

rcParams['figure.max_open_warning'] = 0
rcParams['font.serif'] = ['Palatino'] + rcParams['font.serif']
rcParams['font.family'] = ['serif']
rcParams['mathtext.fontset'] = 'dejavuserif'
rcParams['axes.formatter.use_mathtext'] = True

    
from math import sqrt
dy  = sqrt(3)
ddy = .5
# ====================================================================
def coords(c,r):
    return c+0.5*(r % 2), 0.5 * r * dy

# --------------------------------------------------------------------
def straw(ax,c,r):
    from matplotlib.pyplot import Circle
    x, y = coords(c,r)
    ax.add_patch(Circle((x,y),.5, lw=0,color='darkgoldenrod'))
    ax.add_patch(Circle((x,y),.25,lw=0,color='lemonchiffon'))

# --------------------------------------------------------------------
def box_dim(M,N):
    return (M,.5*(dy*(N-1)+2))

# --------------------------------------------------------------------
def box_dima(M,N):
    return (M+.5,.5*(dy*(N-1)+2))

# --------------------------------------------------------------------
def tower_dim(M,N):
    return (M+.5,.5*(dy*(N-1)+2))

# --------------------------------------------------------------------
def tower_path(M,N):
    from math import sin, cos, pi
    from numpy import asarray,vstack as vs
    from pprint import pprint
    ddx = .5
    ddy = dy/6
    sw  = asarray((-ddx, -ddy))
    nw  = asarray((-ddx,  ddy))
    n   = asarray((   0,  1/dy) )
    ne  = asarray(( ddx,  ddy))
    se  = asarray(( ddx, -ddy))
    s   = asarray((   0, -1/dy) )

    rr     = list(range(0,N))
    cr     = list(range(0,M))
    left   = [(sw+coords(0,r),   nw+coords(0,r))   for r in rr]
    top    = [(n +coords(c,N-1), ne+coords(c,N-1)) for c in cr]
    right  = [(ne+coords(M-1,r), se+coords(M-1,r)) for r in rr[::-1]]
    bottom = [(s +coords(c,0),   sw+coords(c,0))   if c != 0 else
              s +coords(c,0)
              for c in cr[::-1]]

    ll = vs((*left,*top,*right,*bottom))
    
    return ll#(left+top+right+bottom);


def plot_tower_path(M,N):
    from matplotlib.pyplot import gca
    from matplotlib.patches import RegularPolygon, Circle
    from pprint import pprint

    ax = gca();
    a  = tower_path(M,N)
    pprint(a)
    ax.plot(a[:,0],a[:,1])
    for c in range(M):
        for r in range(N):
            x,y = coords(c,r)
            ax.plot(x,y,'or')
            print(f'[{c},{r}] -> ({x:8f},{y:8f})')
            hex = RegularPolygon((x,y),numVertices=6,radius=1/dy,
                                 fill=None,edgecolor='g')
            cir = Circle((x,y),radius=.5,fill=None,edgecolor='m')
            ax.add_patch(hex)
            ax.add_patch(cir)
    ax.figure.show()
    
# --------------------------------------------------------------------
def make_ax(M,N,extraX=0,extraY=0):
    from matplotlib.pyplot import gca
    ax = gca()
    ax.set_aspect(1)
    ax.set_axis_off()
    bw, bh = box_dim(M,N)
    ax.set_xlim(-.5-extraX,bw-.5+extraX)
    ax.set_ylim(-.5-extraY,bh-.5+extraY)
    ax.set_aspect(1)
    return ax

# --------------------------------------------------------------------
def make_axa(M,N,extraX=0,extraY=0):
    from matplotlib.pyplot import gca
    ax = gca()
    ax.set_aspect(1)
    ax.set_axis_off()
    bw, bh = box_dima(M,N)
    ax.set_xlim(-.5-extraX,bw-.5+extraX)
    ax.set_ylim(-.5-extraY,bh-.5+extraY)
    ax.set_aspect(1)
    return ax

# --------------------------------------------------------------------
def make_grid(M,N,extraX=0,extraY=0):
    ax = make_ax(M,N,extraX,extraY)
    for r in range(N):
        nc = M - (r % 2)
        for c in range(nc):
            straw(ax,c,r)
            
    return ax

# --------------------------------------------------------------------
def make_grida(M,N,extraX=0,extraY=0):
    ax = make_axa(M,N,extraX,extraY)
    for r in range(N):
        for c in range(M):
            straw(ax,c,r)
            
    return ax

# --------------------------------------------------------------------
def measure(ax,x1,y1,x2,y2,left,text,**kwargs):
    from matplotlib.pyplot import Line2D, Text
    from math import isclose
    vert = isclose(x1,x2)
    dx   = .2 * ((-1 if left else +1) if vert else 0)
    dy   = .2 * ((-1 if left else +1) if not vert else 0)
    x    = [x1+dx,x1,x2,x2+dx]
    y    = [y1+dy,y1,y2,y2+dy]
    c    = kwargs.pop('color','k')
    ax.add_artist(Line2D(x,y,color=c,**kwargs))
    ax.annotate(text,((x1+x2)/2,(y2+y1)/2),((x1+x2)/2-dx/2,(y2+y1)/2-dy/2),
               ha=(('left' if left else 'right') if vert else 'center'),
               va=(('bottom' if left else 'top') if not vert else 'center'))
    

# ====================================================================
def fig1():
    from matplotlib.pyplot import Line2D, Arrow
    from matplotlib.patches import Arc
    from math import cos, sin, radians
    ax = make_grid(5,4,0,0)
    measure(ax, 0,-0.8, 0.5,-0.8,False,r'$\delta x=R$')
    measure(ax, 0,-1.5, 1,  -1.5,False,r'$\Delta x=D$')
    measure(ax,-1, 0,  -1,   dy/2, False,r'$\Delta y=D\sin\vartheta$ ')
    c = [coords(*cr) for cr in [(0,1),(0,0),(1,0)]]
    x = [xy[0] for xy in c]
    y = [xy[1] for xy in c]
    l = Line2D(x,y,color='k',ls='--')
    a = Arc(c[1],.8,.8,theta1=0,theta2=60,color='k',ls='--')
    ax.add_artist(l)
    ax.add_artist(a)
    ax.annotate(r'$\vartheta$',
                (c[1][0]+.4*cos(radians(10)),
                 c[1][1]+.4*sin(radians(10))),
                (c[1][0]+1*cos(radians(130)),
                 c[1][1]+1*sin(radians(130))),
                arrowprops=dict(arrowstyle="->",ls='--'))
    xl = ax.get_xlim()
    yl = ax.get_ylim()
    ax.set_xlim(xl[0]-2,  xl[1])
    ax.set_ylim(yl[0]-1.5,yl[1])

# --------------------------------------------------------------------
def fig2():
    from matplotlib.patches import Rectangle
    ax = make_grid(11,11,1,1)
    bw, bh = box_dim(11,11)
    measure(ax, 0,-1, bw-1,   -1,   False,r'$M$ columns')
    measure(ax,-1, 0,  -1,    bh-1, False,r'$N$ rows')
    measure(ax,-.5,bh, bw-.5, bh,   True, r'$w$')
    measure(ax,bw, -.5,  bw,  bh-.5,True, r'$h$')
    ax.add_artist(Rectangle((-.5,-.5),bw,bh,color='gray',fill=None))
    xl = ax.get_xlim()
    yl = ax.get_ylim()
    ax.set_xlim(xl[0]-1.2, xl[1])

# --------------------------------------------------------------------
def fig2a():
    from matplotlib.patches import Rectangle
    ax = make_grida(11,11,1,1)
    bw, bh = box_dima(11,11)
    measure(ax, 0,-1, bw-1,   -1,   False,r'$M$ columns')
    measure(ax,-1, 0,  -1,    bh-1, False,r'$N$ rows')
    measure(ax,-.5,bh, bw-.5, bh,   True, r'$w$')
    measure(ax,bw, -.5,  bw,  bh-.5,True, r'$h$')
    ax.add_artist(Rectangle((-.5,-.5),bw,bh,color='gray',fill=None))
    xl = ax.get_xlim()
    yl = ax.get_ylim()
    ax.set_xlim(xl[0]-1.2, xl[1])
    
# --------------------------------------------------------------------
def fig3():
    from matplotlib.patches import Rectangle
    from matplotlib.pyplot import Circle
    ax = make_ax(11,11,1,1)
    bw, bh = box_dim(11,11)
    measure(ax,-.5,bh, bw-.5,     bh,True,   r'$w$')
    measure(ax,bw, -.5,  bw,     bh-.5,True, r'$h$')
    straw(ax,0,0)
    ax.add_artist(Rectangle((-.5,-.5),bw,bh,color='k',fill=None))
    ax.add_patch(Circle((bw/2-.5,bh/2-.5),.1, color='k'))
    ax.annotate(r'$(0,0)$',
                (bw/2-.5,bh/2-.5),
                (bw/2-.5+.2,bh/2-.5+.2))
    ax.add_patch(Circle((-.5,-.5),.1, color='k'))
    ax.annotate(r'$L=(-\frac{w}{2},-\frac{h}{2})$',
                (-.5,-.5),
                (-.5-.2,-.5-.2),ha='right',va='top')
    
    measure(ax,-.5,-1, 0,  -1,False,r'$R$')
    measure(ax,-1, -.5,  -1, 0,False,r'$R$')
    
    xl = ax.get_xlim()
    yl = ax.get_ylim()
    ax.set_xlim(xl[0]-2, xl[1])

# --------------------------------------------------------------------
def fig4():
    from matplotlib.patches import Rectangle
    ax     = make_grid(11,11,1,1)
    bw, bh = box_dim  (11,11)
    tw, th = tower_dim(4,3)
    measure(ax,-.5,-1, tw-.5, -1,   False, r'$w_t$')
    measure(ax,-1, -.5,-1,    th-.5,False, r'$h_t$')
    ax.add_artist(Rectangle((-.5,-.5),  bw,bh,color='gray',fill=None))
    ax.add_artist(Rectangle((-.5,-.5),  tw,th,color='r',lw=2,fill=None))
    ax.add_artist(Rectangle((-1+tw,-.5),tw,th,color='r',lw=2,ls='--',fill=None))
    # ax.annotate(r'$(0,0)$',
    #             (bw/2-.5,bh/2-.5),
    #             (bw/2-.5+.2,bh/2-.5+.2))
    # ax.add_patch(Circle((-.5,-.5),.1, color='k'))
    # ax.annotate(r'$L=(-\frac{w}{2},-\frac{h}{2})$',
    #             (-.5,-.5),
    #             (-.5-.2,-.5-.2),ha='right',va='top')
    # measure(ax,-.5,-1, 0,  -1,False,r'$R$')
    # measure(ax,-1, -.5,  -1, 0,False,r'$R$')
    
    xl = ax.get_xlim()
    yl = ax.get_ylim()
    ax.set_xlim(xl[0]-2, xl[1])

# --------------------------------------------------------------------
def prep():
    from matplotlib.pyplot import ion

    ion()
    
# --------------------------------------------------------------------
#
# EOF
#
