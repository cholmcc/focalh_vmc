//____________________________________________________________________ 
//  
//  FoCAL Virtual Monte-Carlo Simulation
//  Copyright (C) 2023 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    FocalH.C
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon 06 Mar 2023 09:18:28 PM CET
    @brief   Utility functions shared between different codes. */
#ifndef __FocalH__
#define __FocalH__
#include <TObject.h>
#include <map>
#include <iostream>
#include <iomanip>

/** Some utility function shared between codes */
class FocalH : public TObject
{
public:
  /** Square root of three */
  constexpr static double sqrt3 = 1.7320508075688772;
  /** Square root of three divided by 2 (sin 60degrees) */
  constexpr static double sqrt3over2 = sqrt3 / 2;
  //------------------------------------------------------------------
  /** @{
      @name Encoding and decoding of copy numbers
  */
  /** Encode module column copy (bits 24 to 31) */
  static unsigned int ModuleColToCopy(unsigned short modCol)
  {
    return (modCol & 0xFF) << 24;
  }
  /** Decode module column from copy (bits 24 to 31) */
  static unsigned short CopyToModuleCol(unsigned int copy)
  {
    return (copy >> 24) & 0xFF;
  }
  /** Encode module row copy (bits 16 to 23) */
  static unsigned int ModuleRowToCopy(unsigned short modRow)
  {
    return (modRow & 0xFF) << 16;
  }
  /** Decode module row from copy (bits 23 to 16) */
  static unsigned short CopyToModuleRow(unsigned int copy)
  {
    return (copy >> 16) & 0xFF;
  }
  /** Encode tube column copy (bits 8 to 15) */
  static unsigned int TubeColToCopy(unsigned short col)
  {
    return (col & 0xFF) << 8;
  }
  /** Decode tube column from copy (bits 8 to 15) */
  static unsigned short CopyToTubeCol(unsigned int copy)
  {
    return (copy >> 8) & 0xFF;
  }
  /** Encode tube row copy (bits 0 to 7) */
  static unsigned int TubeRowToCopy(unsigned short row)
  {
    return (row & 0xFF);
  }
  /** Decode tube row from copy (bits 0 to 7) */
  static unsigned short CopyToTubeRow(unsigned int copy)
  {
    return copy & 0xFF;
  }
  /** Encode coordinates into a copy number.  The column is put in the
      upper 16 bit, and the row in the lower 16 bit.

      @param col Column number
      @param row Row number 
   */
  static unsigned int CoordToCopy(unsigned short modCol,
				  unsigned short modRow,
				  unsigned short col,
				  unsigned short row)
  {
    return (ModuleColToCopy(modCol) | ModuleRowToCopy(modRow) |
	    TubeColToCopy(col)      | TubeRowToCopy(row));
    // return (row & 0xFFFF) | ((col & 0xFFFF) << 16);
  }
  /** Decode copy number into coordinates.  The column number is taken
      from the upper 16 bit and the row number from the lower 16 bits.

      @param copy       Copy number encoding column and row
      @param moduleCol  On return, module column number
      @param moduleRow  On return, module row number
      @param tubeCol    On return, the column number
      @param tubeRow    On return, the row number 
   */
  static void CopyToCoord(unsigned int    copy,
			  unsigned short& moduleCol,
			  unsigned short& moduleRow,
			  unsigned short& tubeCol,
			  unsigned short& tubeRow) 
  {
    moduleCol = CopyToModuleCol(copy);
    moduleRow = CopyToModuleRow(copy);
    tubeCol   = CopyToTubeCol  (copy);
    tubeRow   = CopyToTubeRow  (copy);
  }
  /** @} */
  //------------------------------------------------------------------
  /** @{
      @name Parameters 
  */
  /** Number of module columns */
  unsigned short     fNModuleCol    = 3;
  /** Number of module rows per module rows */
  unsigned short     fNModuleRow    = 3;
  /** Number of tube columns */
  unsigned short     fNTubeCol      = 24;
  /** Number of tube rows per module rows */
  unsigned short     fNTubeRow      = 28;
  /** Outer tube radius in cm*/
  double             fTubeRadius    = 0.25 / 2;
  /** Fibre radius in cm*/
  double             fScintRadius   = 0.12 / 2;
  /** Space between tube and fibre in cm */
  double             fEpsilon       = 0.02 / 2;
  /** Length of straws in cm */
  double             fLength        = 110;
  /** Max integration time in seconds */
  double             fMaxTime       = 1e-6;
  /** Thickness of box surrounding the straws in cm */
  double             fBoxThick      = 0.1;
  /** Whether to enable fast optical fibre transport */
  bool               fFast          = false;
  /** full rows */
  bool               fFull          = true;
  /** Print information */
  void Print(Option_t* ="") const
  {
    std::cout << "FoCAL-H parameters:\n"
	      << " Size:          "
	      << GetWidth() << "cm x "
	      << GetHeight() << "cm x "
	      << fLength << "cm\n"
	      << " Straws:        " << fNTubeCol << " x " << fNTubeRow << "\n"
	      << " Modules:       " << fNModuleCol << " x " << fNModuleRow << "\n"
	      << " Tube radius:   " << fTubeRadius << "cm\n"
	      << " Fibre radius:  " << fScintRadius << "cm\n"
	      << " Length:        " << fLength << "cm\n"
	      << " Spacing:       " << fEpsilon << "cm\n"
	      << " Max time:      " << fMaxTime << "s\n"
	      << " Box thickness: " << fBoxThick << ""
	      << std::endl;
  } //*MENU*
  /** @} */
  //------------------------------------------------------------------
  /** @{
      @name Get spatial coordinates 
  */
  /** Get Tube spatial coordinates (relative to module) from detector
      coordinates.  The spatial coordinates, relative to the mother
      volume, in the transverse direction are calculated from the
      column and row numbers.

      @param x      On return, the x coordinate 
      @param y      On return, the x coordinate
      @param c      Tube column number
      @param r      Tube row number
   */
  void TubeXY(double&         x,
	      double&         y,
	      unsigned short  c,
	      unsigned short  r) const
  {
    x = fTubeRadius *              (2 * c + 1 - fNTubeCol + (r % 2) - (fFull ? .5:0));
    y = fTubeRadius * sqrt3over2 * (2 * r + 1 - fNTubeRow);
  }
  /** Get Module spatial coordinates from detector coordinates.  The
      spatial coordinates, relative to the mother volume, in the
      transverse direction are calculated from the column and row
      numbers.

      @param moduleCol   Module column number
      @param moduleRow   Module row number
      @param radius      Outer radius of absorber tubes
      @param nModuleCol  Number of module columns
      @param nModuleRow  Number of module rows
      @param nTubeCol    Number of tube columns in module 
      @param nTubeRow    Number of tube rows in module
      @param boxThick    Thickness of box 
      @param x           On return, the x coordinate 
      @param y           On return, the x coordinate 
      @parma full   If true, use full rows 
   */
  void ModuleXY(double&        x,
		double&        y,
		unsigned short moduleCol,
		unsigned short moduleRow) const
  {
    double w, h;
    ContainerSize(w, h);
    w += 2*fBoxThick;
    h += 2*fBoxThick;
    x =  w * (moduleCol + .5 - .5 * fNModuleCol);
    y =  h * (moduleRow + .5 - .5 * fNModuleRow);    
  }
  /** Get Tube spatial coordinates from detector coordinates.  The
      spatial coordinates, relative to the FoCAL-H mother volume, in
      the transverse direction are calculated from the column and row
      numbers.

      @param tubeCol     Tube column number
      @param tubeRow     Tube row number
      @param moduleCol   Module column number
      @param moduleRow   Module row number
      @param radius      Outer radius of absorber tubes
      @param nModuleCol  Number of module columns
      @param nModuleRow  Number of module rows
      @param nTubeCol    Number of tube columns in module 
      @param nTubeRow    Number of tube rows in module
      @param boxThick    Thickness of box 
      @param x           On return, the x coordinate 
      @param y           On return, the x coordinate 
      @parma full   If true, use full rows 
   */
  void XY(double&        x,
	  double&        y,
	  unsigned short moduleCol,
	  unsigned short moduleRow,
	  unsigned short tubeCol,
	  unsigned short tubeRow) const
  {
    Double_t mx, my, tx, ty;
    ModuleXY(mx, my, moduleCol, moduleRow);
    TubeXY  (tx, ty, tubeCol,   tubeRow);
    x = mx + tx;
    y = my + ty;
  }
  /** Get the transverse size of the containing box.  

      @param radius Outer radius of absorber tubes
      @param ncols  Total number of columns
      @param nrows  Total number of rows
      @param w      On return, the total width along X
      @param h      On return, the total height along Y
      @param full   If false, every second row has one less fibre,
      if true, then all rows have the same number of fibres. 
  */
  void ContainerSize(double&         w,
		     double&         h) const
  {
    w = fTubeRadius * (2 * fNTubeCol + (fFull ? 1 : 0));
    h = fTubeRadius * (sqrt3 * (fNTubeRow - 1) + 2);
  }
  /** Get spatial coordinates from a copy number.

      @param x      On return, the x coordinate 
      @param y      On return, the x coordinate
      @param copy   Copy number 
   */
  void TubeXY(double&        x,
	      double&        y,
	      unsigned int   copy) const
  {
    unsigned short mc, mr, c, r;
    CopyToCoord(copy, mc, mr, c, r);
    TubeXY(x, y, c, r);
  }
  /** Get spatial coordinates from a copy number.

      @param copy        Copy number 
      @param radius      Outer radius of absorber tubes
      @param nModuleCol  Number of module columns
      @param nModuleRow  Number of module rows
      @param nTubeCol    Number of tube columns in module 
      @param nTubeRow    Number of tube rows in module
      @param boxThick    Thickness of box 
      @param x           On return, the x coordinate 
      @param y           On return, the x coordinate 
      @parma full        If true, use full rows 
   */
  void ModuleXY(double&        x,
		double&        y,
		unsigned int   copy) const
  {
    unsigned short moduleCol, moduleRow, c, r;
    CopyToCoord(copy, moduleCol, moduleRow, c, r);
    ModuleXY(x, y, moduleCol, moduleRow);
  }
  /** Get tube spatial coordiantes from copy number.  Coordinates
      returned are relative to FoCAL-H mother volume.

      @param x           On return, the x coordinate 
      @param y           On return, the x coordinate 
      @param copy        Copy number 
  */
  void XY(double&        x,
	  double&        y,
	  unsigned int   copy) const
  {
    unsigned short moduleCol, moduleRow, tubeCol, tubeRow;
    CopyToCoord(copy, moduleCol, moduleRow, tubeCol, tubeRow);
    XY(x, y, moduleCol, moduleRow, tubeCol, tubeRow);
  }
  /** @} */
  //------------------------------------------------------------------
  /** @{
      @name Dimensions
  */
  /** Get total width of detector */
  double GetWidth() const
  {
    double w, h;
    ContainerSize(w, h);
    return (w+2*fBoxThick)*fNModuleCol;
  }
  /** Get total height of detector */
  double GetHeight() const
  {
    double w, h;
    ContainerSize(w, h);
    return (h+2*fBoxThick)*fNModuleRow;
  }
  /** Get the length of the flux volume */
  double   GetFluxLength() const { return fLength+fEpsilon;  }
  /** Get front Z of flux volume */
  double   GetFluxFront()  const { return -GetFluxLength() / 2; }
  /** Get back Z of flux volume */
  double   GetFluxBack()   const { return +GetFluxLength() / 2; }
  /** Get front Z of fibres */
  double   GetFront()      const { return -fLength / 2; }
  /** Get back Z of fibres */
  double   GetBack()       const { return +fLength / 2; }
  /** @} */
  //------------------------------------------------------------------
  /** @{
      @name Set-ups and mapping
  */
  /** Get number of columns in a row */
  unsigned short ColumnsInRow(unsigned short row) const
  {
    return fNTubeCol - (fFull ? 0 : (row % 2));
  }
  typedef std::map<unsigned int,unsigned int> Coord2IndexMap;
  /** Create a map from copy number to index */
  void MakeMapping(Coord2IndexMap& mapping) const
  {
    mapping.clear();
    unsigned int idx = 0;
    for (unsigned short mr = 0; mr < fNModuleRow; mr++) {
      for (unsigned short mc = 0; mc < fNModuleCol; mc++) {
	for (unsigned short tr = 0; tr < fNTubeRow; tr++) {
	  unsigned short nc = ColumnsInRow(tr);
	  for (unsigned short tc = 0; tc < nc; tc++) {
	    unsigned int copy = CoordToCopy(mc,mr,tc,tr);
	    mapping[copy] = idx;
	    idx++;
	  }
	}
      }
    }
  }
  /** @} */
  ClassDef(FocalH,1);
};

#endif
//
// EOF
//

  
  
