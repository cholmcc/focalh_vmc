#
# The files
#
DATA_SOURCES	:= $(srcdir)FocalH.C		\
		   $(srcdir)FocalE.C		\
		   $(srcdir)FocalHHit.C		\
		   $(srcdir)FocalEHit.C		\
		   $(srcdir)FocalHDigit.C	\
		   $(srcdir)FocalHFlux.C	\
		   $(srcdir)FocalHSum.C		\
		   $(srcdir)FocalHRO.C		

SIM_SOURCES	:= $(srcdir)Simulate.C		\
		   $(srcdir)ScintOptical.C	\
		   $(srcdir)AirOptical.C	\
		   $(srcdir)Builder.C		\
		   $(srcdir)SubBuilder.C	\
		   $(srcdir)FrontBack.C		\
		   $(srcdir)FocalHBuilder.C	\
		   $(srcdir)FocalEBuilder.C	\
		   $(srcdir)FocalHHitter.C	\
		   $(srcdir)FocalEHitter.C	\
		   $(srcdir)FocalHFluxer.C	\
		   $(srcdir)FocalHSummer.C		

ANA_SOURCES	:= $(srcdir)Analyse.C		\
		   $(srcdir)Analyser.C		\
		   $(srcdir)Profiler.C		\
		   $(srcdir)Leaving.C		\
		   $(srcdir)FocalHDigitizer.C		

SCRIPTS		:= $(srcdir)CompareProfiles.C	\
		   $(srcdir)LoadData.C		\
		   $(srcdir)LoadSim.C		\
		   $(srcdir)LoadAna.C
#
# EOF
#
